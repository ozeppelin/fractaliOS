//
//  BeersLikeViewController.swift
//  fractal
//
//  Created by Anderson Silva on 13/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

class BeersLikeViewController: BaseViewController {
    
    var presenterInterface : BeersPresenterInterface!
    var total = 0
    var beersObj:[Beers]?
    
    @IBOutlet weak var tbView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Favoritos"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showLoading()
        BeersWireframe.configureFavorite(viewController: self)
        presenterInterface.getBeersFavorite()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadXib() {
        self.tbView.isHidden = false
        let cellNib = UINib(nibName: "BeersTableViewCell", bundle: nil)
        self.tbView.register(cellNib, forCellReuseIdentifier: "beersCell")
        
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)
        self.tbView.reloadData()
        self.hideLoading()
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgDetailFavorite" {
            if let vc = segue.destination as? BeersDetailViewController, let beers = sender as? Beers {
                vc.objBeer = beers
            }
        }
        
    }
 

}

extension BeersLikeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "beersCell", for: indexPath) as! BeersTableViewCell
        cell.setupFavorite(beer: self.beersObj![indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgDetailFavorite", sender: self.beersObj![indexPath.row])
    }
    
}

extension BeersLikeViewController: BeersPresenterViewInterface {
    func fetchBeers(beers:[Beers]) {
        self.total = beers.count
        self.beersObj = beers
        self.loadXib()
    }
    
    func beersError(error:String) {
        self.tbView.isHidden = true
        self.alertMessage(title: "Beers", msg: error, btn: "OK")
        self.hideLoading()
    }
    
    func beersNotFound(msg:String) { }
}

