//
//  BeersPresenter.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import UIKit

protocol BeersPresenterInterface : class {
    func getBeers()
    func getBeersFavorite()
    func getSearch(beers:[Beers], searchText:String)
    func checkFavorite(beer:Beers, cell:BeersTableViewCell)
    func setFavorite(beer:Beers, cell:BeersTableViewCell)
}

protocol BeersPresenterViewInterface : class {
    func fetchBeers(beers:[Beers])
    func beersError(error:String)
    func beersNotFound(msg:String)
}

class BeersPresenter: BeersPresenterInterface, BeersInteractorOutput {
    
    private(set) var interactor:BeersInteractorInput
    private(set) var wireframe:BeersWireframe
    
    weak var beersPresenterViewInterface : BeersPresenterViewInterface?

    
    init(interactor:BeersInteractor, wireframe:BeersWireframe) {
        self.interactor = interactor
        self.wireframe = wireframe
    }
    
    //MARK: - BeersPresenterInterface
    func getBeers() {
        self.interactor.beersAll()
    }
    
    func getBeersFavorite() {
        self.interactor.beersFavorite()
    }
    
    func getSearch(beers:[Beers], searchText:String) {
        if searchText.count > 0 {
            self.interactor.searchBeers(beers: beers, searchText: searchText)
        }else{
            self.getBeers()
        }
    }
    
    func checkFavorite(beer:Beers, cell:BeersTableViewCell) {
        var imageLike:UIImage?
        imageLike = UIImage(named: "ico_favorite_desable")
        
        if self.interactor.checkFavorite(beer: beer) {
            imageLike = UIImage(named: "ico_favorite_enable")
        }
        cell.btLike.setImage(imageLike, for: .normal)
    }
    
    func setFavorite(beer:Beers, cell:BeersTableViewCell) {
        var imageLike:UIImage?
        if self.interactor.favorite(beer: beer) {
            imageLike = UIImage(named: "ico_favorite_enable")
        }else{
            imageLike = UIImage(named: "ico_favorite_desable")
        }
        cell.btLike.setImage(imageLike, for: .normal)
    }
    
    //MARK: - BeersInteractorOutput
    func resultBeers(beers:[Beers]) {
        if beers.count > 0 {
            self.beersPresenterViewInterface?.fetchBeers(beers: beers)
        }else{
            self.beersPresenterViewInterface?.beersError(error: "Não há registros.")
        }
    }
    
    func beerError(error:Error) {
        self.beersPresenterViewInterface?.beersError(error: error.localizedDescription)
    }
    
    func searchNotFound(msg:String) {
        self.beersPresenterViewInterface?.beersNotFound(msg: msg)
    }
}
