//
//  BeersDetailViewController.swift
//  fractal
//
//  Created by Anderson Silva on 13/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

class BeersDetailViewController: BaseViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbTagline: UILabel!
    @IBOutlet weak var lbDescricao: UITextView!
    
    var objBeer:Beers!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addBackButton()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadData() {
        
        self.title = self.objBeer.name
        if let photo = objBeer.imageUrl {
            self.img.kf.setImage(with: URL(string: photo), placeholder: UIImage(named: "placeholderImg"), options: [.transition((.fade(1)))], progressBlock: nil, completionHandler: nil)
        }
        
        self.lbTitulo.text = self.objBeer.name
        self.lbTagline.text = self.objBeer.tagline
        self.lbDescricao.text = self.objBeer.descriptionValue
        
    }

}
