//
//  BeersProvider.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class BeersProvider: NSObject {
    
    func getBeers(completion:@escaping(_ beers:[Beers]?, _ error:Error?) -> Void) -> Void {
        
        request(RouterAPI.readBeers)
            .validate(contentType: ["application/json"])
            .responseArray { (response:DataResponse<[Beers]>) in
                switch response.result {
                case .success(let beers) : completion(beers, nil); break;
                case .failure(let error) : completion(nil, error); break;
                }
        }
        
    }
    
}
