//
//  BeersViewController.swift
//  fractal
//
//  Created by Anderson Silva on 13/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

class BeersViewController: BaseViewController {

    var presenterInterface : BeersPresenterInterface!
    var total = 0
    var beersObj:[Beers]?
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var msgSearch: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.showLoading()
        self.msgSearch.isHidden = true
        self.title = "Beers List"
        self.searchBar.delegate = self
        BeersWireframe.configure(viewController: self)
        presenterInterface.getBeers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadXib() {
        self.tbView.isHidden = false
        let cellNib = UINib(nibName: "BeersTableViewCell", bundle: nil)
        self.tbView.register(cellNib, forCellReuseIdentifier: "beersCell")
        
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)
        self.tbView.reloadData()
        self.hideLoading()
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgDetail" {
            if let vc = segue.destination as? BeersDetailViewController, let beers = sender as? Beers {
                vc.objBeer = beers
            }
        }
        
    }
 

}
extension BeersViewController: UITableViewDelegate, UITableViewDataSource, BeersTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "beersCell", for: indexPath) as! BeersTableViewCell
        cell.delegate = self
        cell.setup(beer: self.beersObj![indexPath.row], presenterInterface: self.presenterInterface)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgDetail", sender: self.beersObj![indexPath.row])
    }
    
    func setLike(in cell:BeersTableViewCell) {
        if let indexPath = self.tbView.indexPath(for: cell) {
            self.presenterInterface.setFavorite(beer: self.beersObj![indexPath.row], cell: cell)
        }
    }
    
}

extension BeersViewController: BeersPresenterViewInterface {
    func fetchBeers(beers:[Beers]) {
        self.total = beers.count
        self.beersObj = beers
        self.loadXib()
    }
    
    func beersError(error:String) {
        self.alertMessage(title: "Beers", msg: error, btn: "OK")
        self.hideLoading()
    }
    
    func beersNotFound(msg:String) {
        self.tbView.isHidden = true
        self.msgSearch.isHidden = false
        self.msgSearch.text = msg
        self.hideLoading()
    }
}

extension BeersViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.returnKeyType = .done
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.showLoading()
        if let beers = self.beersObj {
            self.presenterInterface.getSearch(beers: beers, searchText: searchText)
        }
    }
    
}
