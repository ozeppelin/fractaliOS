//
//  BeersWireframe.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

class BeersWireframe {
    
    var presenter : BeersPresenter?
    
    // MARK: Navigation
    
    static func configure(viewController: BeersViewController) {
        // Generate Module Components
        let interactor = BeersInteractor()
        let wireframe = BeersWireframe()
        let presenter = BeersPresenter(interactor: interactor, wireframe: wireframe)
        
        // Wire Up
        viewController.presenterInterface = presenter
        interactor.output = presenter
        presenter.beersPresenterViewInterface = viewController
    }
    
    static func configureFavorite(viewController: BeersLikeViewController) {
        // Generate Module Components
        let interactor = BeersInteractor()
        let wireframe = BeersWireframe()
        let presenter = BeersPresenter(interactor: interactor, wireframe: wireframe)
        
        // Wire Up
        viewController.presenterInterface = presenter
        interactor.output = presenter
        presenter.beersPresenterViewInterface = viewController
    }
    
}
