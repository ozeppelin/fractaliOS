//
//  BeersInteractor.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

protocol BeersInteractorInput: class {
    func beersAll()
    func beersFavorite()
    func favorite(beer:Beers) -> Bool
    func checkFavorite(beer:Beers) -> Bool
    func searchBeers(beers:[Beers], searchText:String)
}

protocol BeersInteractorOutput: class {
    func resultBeers(beers:[Beers])
    func beerError(error:Error)
    func searchNotFound(msg:String)
}

class BeersInteractor: NSObject, BeersInteractorInput {
    
    weak var output: BeersInteractorOutput?
    
    private lazy var provider: BeersProvider = {
        return BeersProvider.init()
    }()
    
    private lazy var providerRealm: BeersRealmFavoriteDB = {
        return BeersRealmFavoriteDB.init()
    }()
    
    func beersAll() {
        provider.getBeers { (beers, error) in
            guard error == nil else {
                self.output?.beerError(error: error!)
                return
            }
            
            self.output?.resultBeers(beers: beers!)
        }
    }
    
    func beersFavorite() {
        self.output?.resultBeers(beers: providerRealm.list())
    }
    
    func favorite(beer:Beers) -> Bool {
        return providerRealm.saveFavorite(beer: beer)            
    }
    
    func checkFavorite(beer:Beers) -> Bool {
        return providerRealm.checkFavorite(beer:beer)
    }
    
    func searchBeers(beers:[Beers], searchText:String) {
        
        if searchText.count > 0 {
            if searchText.trim().count >= 2 {
                
                var objFiltered:[Beers] = [Beers]()
                
                objFiltered = beers.filter({ (text) -> Bool in
                    let tmp:NSString = text.name! as NSString
                    let range = tmp.range(of: searchText, options: .caseInsensitive)
                    return (range.location != NSNotFound)
                })
                
                if objFiltered.count > 0 {
                    self.output?.resultBeers(beers: objFiltered)
                }else{
                    let result = "\"\(searchText)\""
                    self.output?.searchNotFound(msg: "A Cerveja \(result) não foi encontrada.")
                }
                
            }else{
                self.output?.searchNotFound(msg: "Informe ao menos dois caracteres para a busca.")
            }
        }else{
            self.output?.searchNotFound(msg: "Informe ao menos dois caracteres para a busca.")
        }
        
    }
    
}
