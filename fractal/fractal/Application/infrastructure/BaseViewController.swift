//
//  BaseViewController.swift
//  fractal
//
//  Created by Anderson Silva on 13/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

class BaseViewController: UIViewController {
    
    var loadingView : UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        let recognizer:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(BaseViewController.backButton))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewGestureNoAction(){
        let recognizer:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: nil)
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func addBackButton() {
        let barButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backButton))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backButton(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func alertMessage(title:String, msg:String, btn:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: btn, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoading() {
        DispatchQueue.main.async {
            self.loadingView = UIStoryboard(name: "Loading", bundle: nil).instantiateViewController(withIdentifier: "LoadingView")
            self.loadingView!.view.alpha = 0.0
            self.view.addSubview(self.loadingView!.view)
            
            UIView.animate(withDuration: 0.5, animations: {
                self.loadingView!.view.alpha = 0.8
            })
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.8, animations: {
                self.loadingView!.view.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    self.loadingView?.view.removeFromSuperview()
                    self.loadingView = nil
                }
            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
