//
//  RouterAPI.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation

import Alamofire

enum RouterAPI : URLRequestConvertible {
    
    //BEERS
    case readBeers
    
    static let baseURL = "https://api.punkapi.com/"
    
    var method : HTTPMethod {
        switch self {
        //FAQ
        case .readBeers: return .get
        }
    }
    
    var path : String {
        switch self {
        //FAQ
        case .readBeers:
            return "v2/"
        }
    }
    
    var subPath : String {
        switch self {
        case .readBeers:
            return path + "beers/"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try RouterAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(subPath))
        urlRequest.httpMethod = method.rawValue
        
        print("URL_REQUEST => ", urlRequest)
        
        return urlRequest
    }
    
}
