//
//  HopsRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class HopsRealm: Object {
    
    @objc dynamic var attribute: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var add: String = ""
    @objc dynamic var amount: AmountRealm? = nil
    
    override class func primaryKey() -> String? {
        return "name"
    }

    convenience required public init(copy obj: Hops) {
        self.init()
        
        if let attribute = obj.attribute {
            self.attribute = attribute
        }
        
        if let name = obj.name {
            self.name = name
        }
        
        if let add = obj.add {
            self.add = add
        }
        
        if let amount = obj.amount {
            self.amount = AmountRealm(copy: amount)
        }
        
    }
}
