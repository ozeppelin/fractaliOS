//
//  AmountRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class AmountRealm: Object {
    
    @objc dynamic var unit:String = ""
    @objc dynamic var value:Int = 0
    
    override class func primaryKey() -> String? {
        return "unit"
    }
    
    convenience required public init(copy obj: Amount) {
        
        self.init()
        
        if let unit = obj.unit {
            self.unit = unit
        }
        
        if let value = obj.value {
            self.value = value
        }
        
    }
}
