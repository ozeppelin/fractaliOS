//
//  MethodRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class MethodRealm: Object {
    
    @objc dynamic var fermentation: FermentationRealm? = nil
    @objc dynamic var twist: String = ""
    var mashTempObj = List<MashTempRealm>()
    
    override class func primaryKey() -> String? {
        return "twist"
    }
    
    convenience required public init(copy obj: Method) {
        self.init()
        
        if let fermentation = obj.fermentation {
            self.fermentation = FermentationRealm(copy: fermentation)
        }
        
        if let twist = obj.twist {
            self.twist = twist
        }
        
        if let mashTemp = obj.mashTemp {
            if mashTemp.count > 0 {
                for value in mashTemp {
                    self.mashTempObj.append(MashTempRealm(copy: value))
                }
            }
        }
        
    }
}
