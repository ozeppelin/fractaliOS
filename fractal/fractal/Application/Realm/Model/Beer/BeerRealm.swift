//
//  BeerRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class FoodPairingRealm: Object {
    @objc dynamic var beers: Int = 0
    @objc dynamic var foodPairing: String = ""
    
    convenience required public init(name:String, beer:Int) {
        self.init()
        self.foodPairing = name
        self.beers = beer
    }
}

class BeerRealm: Object {
    
    @objc dynamic var ingredients: IngredientsRealm? = nil
    @objc dynamic var srm: Int = 0
    @objc dynamic var method: MethodRealm? = nil
    @objc dynamic var name: String = ""
    @objc dynamic var ebc: Int = 0
    @objc dynamic var contributedBy: String = ""
    @objc dynamic var ibu: Int = 0
    @objc dynamic var targetOg: Int = 0
    @objc dynamic var targetFg: Int = 0
    @objc dynamic var abv: Float = 0
    @objc dynamic var volume: VolumeRealm? = nil
    @objc dynamic var ph: Float = 0
    @objc dynamic var boilVolume: BoilVolumeRealm? = nil
    @objc dynamic var firstBrewed: String = ""
    @objc dynamic var brewersTips: String = ""
    @objc dynamic var descriptionValue: String = ""
    @objc dynamic var id: Int = 0
    @objc dynamic var attenuationLevel: Int = 0
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var tagline: String = ""
    var foodPairingObj = List<FoodPairingRealm>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience required public init(copy obj: Beers) {
        self.init()
        
        if let ingredients = obj.ingredients {
            self.ingredients = IngredientsRealm(copy: ingredients)
        }
        
        if let srm = obj.srm {
            self.srm = srm
        }
        
        if let method = obj.method {
            self.method = MethodRealm(copy: method)
        }
        
        if let name = obj.name {
            self.name = name
        }
        
        if let ebc = obj.ebc {
            self.ebc = ebc
        }
        
        if let contributedBy = obj.contributedBy {
            self.contributedBy = contributedBy
        }
        
        if let ibu = obj.ibu {
            self.ibu = ibu
        }
        
        if let targetOg = obj.targetOg {
            self.targetOg = targetOg
        }
        
        if let abv = obj.abv {
            self.abv = abv
        }
        
        if let volume = obj.volume {
            self.volume = VolumeRealm(copy: volume)
        }
        
        if let ph = obj.ph {
            self.ph = ph
        }
        
        if let boilVolume = obj.boilVolume {
            self.boilVolume = BoilVolumeRealm(copy: boilVolume)
        }
        
        if let firstBrewed = obj.firstBrewed {
            self.firstBrewed = firstBrewed
        }
        
        if let brewersTips = obj.brewersTips {
            self.brewersTips = brewersTips
        }
        
        if let descriptionValue = obj.descriptionValue {
            self.descriptionValue = descriptionValue
        }
        
        if let id = obj.id {
            self.id = id
        }
        
        if let foodPairing = obj.foodPairing {
            if foodPairing.count > 0 {
                for value in foodPairing {
                    self.foodPairingObj.append(FoodPairingRealm(name: value, beer: self.id))
                }
            }
        }
        
        if let attenuationLevel = obj.attenuationLevel {
            self.attenuationLevel = attenuationLevel
        }
        
        if let imageUrl = obj.imageUrl {
            self.imageUrl = imageUrl
        }
        
        if let tagline = obj.tagline {
            self.tagline = tagline
        }
        
    }
}
