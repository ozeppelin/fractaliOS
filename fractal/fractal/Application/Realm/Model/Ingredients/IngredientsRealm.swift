//
//  IngredientsRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class IngredientsRealm: Object {
    
    @objc dynamic var yeast:String = ""
    var maltObj = List<MaltRealm>()
    var hopsObj = List<HopsRealm>()
    
    override class func primaryKey() -> String? {
        return "yeast"
    }
    
    convenience required public init(copy obj: Ingredients) {
        self.init()
        
        if let yeast = obj.yeast {
            self.yeast = yeast
        }
        
        if let malt = obj.malt {
            if malt.count > 0 {
                for value in malt {
                    self.maltObj.append(MaltRealm(copy: value))
                }
            }
        }
        
        if let hops = obj.hops {
            if hops.count > 0 {
                for v in hops {
                    self.hopsObj.append(HopsRealm(copy: v))
                }
            }
        }
        
    }
}
