//
//  MaltRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class MaltRealm: Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var amount: AmountRealm? = nil
    
    override class func primaryKey() -> String? {
        return "name"
    }
    
    convenience required public init(copy obj: Malt) {
        self.init()
        
        if let name = obj.name {
            self.name = name
        }
        
        if let amount = obj.amount {
            self.amount = AmountRealm(copy: amount)
        }
    }
    
}
