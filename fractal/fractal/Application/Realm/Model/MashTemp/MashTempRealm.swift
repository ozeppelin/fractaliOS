//
//  MashTempRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class MashTempRealm: Object {
    
    @objc dynamic var temp: TempRealm? = nil
    @objc dynamic var duration: Int = 0
    
    override class func primaryKey() -> String? {
        return "duration"
    }
    
    convenience required public init(copy obj: MashTemp) {
        self.init()
        
        if let temp = obj.temp {
            self.temp = TempRealm(copy: temp)
        }
        
        if let duration = obj.duration {
            self.duration = duration
        }
        
    }
}
