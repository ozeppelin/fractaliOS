//
//  FermentationRealm.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class FermentationRealm: Object {
    
    @objc dynamic var temp: TempRealm? = nil
    
    convenience required public init(copy obj: Fermentation) {
        self.init()
        
        if let temp = obj.temp {
            self.temp = TempRealm(copy: temp)
        }
        
    }
    
}
