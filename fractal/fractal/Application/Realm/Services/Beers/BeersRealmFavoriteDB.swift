//
//  BeersRealmFavoriteDB.swift
//  fractal
//
//  Created by Anderson Silva on 11/06/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class BeersRealmFavoriteDB: NSObject {
    
    let realm = try! Realm()
    
    func saveFavorite(beer:Beers) -> Bool {
        
        var value : Bool = false
        let objRealm = BeerRealm(copy: beer)
        if !self.checkFavorite(beer: beer) {
            value = self.saveBeerFavorite(beer: objRealm)
        }else{
            value = self.deleteFavorite(beer: objRealm)
        }
        
        return value
    }
    
    func removeFavorite(beer:Beers) -> Bool {
        let beersRealm = BeerRealm(copy: beer)
        return self.deleteFavorite(beer: beersRealm)
    }
    
    func checkFavorite(beer:Beers) -> Bool {
        var value:Bool = false
        let objBeer = self.list()
        if objBeer.count > 0 {
            let objFilter = objBeer.filter{ $0.id == beer.id }
            if objFilter.count > 0 {
                value = true
            }
        }
        
        return value
        
    }
    
    func list() -> [Beers] {
        return self.listBeersFavorite()
    }
    
    //MARK: PRIVATES
    private func deleteFavorite(beer:BeerRealm) -> Bool {
        var value : Bool = true
        do {
            try! realm.write {
                realm.delete(realm.objects(BeerRealm.self).filter("id=%@",beer.id))
                value = false
            }
        }
        
        return value
    }
    
    private func listBeersFavoriteRealm() -> [BeerRealm] {
        let theaters : [BeerRealm] = realm.objects(BeerRealm.self).map { (theater) -> BeerRealm in
            return theater
        }
        return theaters
    }
    
    private func listBeersFavorite() -> [Beers] {
        let listRealm = self.listBeersFavoriteRealm()
        
        if listRealm.count > 0 {
            var beers = [Beers]()
            for list in listRealm {
                beers.append(Beers(realmFavorite: list))
            }
            return beers
        }
        return [Beers]()
    }
    
    private func saveBeerFavorite(beer:BeerRealm) -> Bool {
        var value : Bool = false
        try! realm.write {
            self.realm.add(beer.self, update: true)
            value = true
        }
        return value
    }
    
}
