# BEERS README


ARQUITETURA: VIPER

App de estudo utilizando arquitetura VIPER

Executar pod install pelo terminal, para a instalação das bibliotecas utilizadas no projeto.

Pods utilizados:
* AlamofireObjectMapper
* Alamofire
* Kingfisher
* Realm

# ARQUITETURA VIPER

![alt tag](https://gitlab.com/ozeppelin/fractaliOS/raw/master/viper.png "VIPER")       
